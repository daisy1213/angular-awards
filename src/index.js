import angular from 'angular';
import pages from './pages/index';  //router-module-name
import components from './components/index'; //app.component-name
import './index.scss';

var app = angular.module('app', [components, pages]);

app.run(['$rootScope', function($rootScope) {

}]);

//因为同一个页面包含多个app,angularjs不会帮你启动，需要手动启动这些app
//手动初始化调用app.bootstrap(element, [app.name])
app.bootstrap = function() {
    angular.element(window.document).ready(function() {
        angular.bootstrap(window.document, [app.name]);
    });
};

app.bootstrap();

export default app;
