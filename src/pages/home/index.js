import template from './template.html';
import style from './style.scss';
// import app from '../../components/'

var controller = ['$scope', function($scope) {
    $scope.style = style;
    $scope.allUsers = [{
        employeeId: '10003',
        name: 'Benlv',
        qualification: true
    },
    {
        employeeId: '10013',
        name: 'Derek',
        qualification: true
    },
    {
        employeeId: '10023',
        name: 'Sherry',
        qualification: true
    },
    {
        employeeId: '10045',
        name: 'Daisy',
        qualification: true
    }];
    
    // console.log('Hahahaahhhahahah...', style);
}];

//定义页面的url
var page = {
    path: '/home',
    template: template,
    controller: controller
};

export default page;
