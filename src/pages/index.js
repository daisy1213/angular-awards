import angular from 'angular';
import ngRoute from 'angular-route';
import homePage from './home/index';

var app = angular.module('app.routers', [ngRoute]);

//路由跳转配置
app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when(homePage.path, {
            template: homePage.template,
            controller: homePage.controller
        }).otherwise('/home');
}]);

export default app.name;
