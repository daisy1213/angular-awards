import template from './template.html';
import style from './style.scss';


//以对象的形式自定义指令，名称和功能函数
export default {
    name: 'userList',
    fn: [
        function () {
            return {
                restrict: 'E',  //书写应该以元素的形式
                template: template, //该指令返回ul列表 ng-repeat
                scope: {
                    awardUsers: '=', //定义scope功能
                    today: '='       // 属性分别伪awardUsers、today，可以在html中使用

                },
                link: function ($scope) {
                    $scope.style = style; //link函数
                }
            };
        }
    ]
};
