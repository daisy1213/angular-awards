import angular from 'angular';
import UserList from './UserList/index';

var app = angular.module('app.components', []);


//自定义指令
app.directive(UserList.name, UserList.fn);

export default app.name;
